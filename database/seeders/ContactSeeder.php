<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'user_id'=>1,
            'phone_no'=>'3335654444',
            'address'=>'Address_Test'

        ]);
    }
}
